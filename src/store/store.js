import { createStore } from 'vuex';

export default createStore({
    state: {
        email: null,
        name: null,
        id: null,
    },
    getters: {
        getUser(state) {
            return { email: state.email, name: state.name, id: state.id };
        }
    },
    mutations: {
        setUser(state, args) {
            state.email = args.email;
            state.name = args.name;
            state.id = args.id;
        }
    },
    actions: {
        setUser(ctx, args) {
            return new Promise((resolve) => {
                ctx.commit('setUser', args);
                resolve(true);
            });
        }
    },
    modules: {
    }
});