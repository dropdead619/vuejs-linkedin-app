import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import UsersPanel from "../views/UsersPanel.vue";
import SearchResults from "../views/SearchResults.vue";
import AuthUser from "../views/AuthUser.vue";
import axios from "axios";
import store from "../store/store.js";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/auth",
    name: "AuthUser",
    component: AuthUser,
  },
  {
    path: "/user/:id/:tab",
    name: "User",
    component: Home,
  },
  {
    path: "/users",
    name: "Users",
    component: UsersPanel,
  },
  {
    path: "/search-results",
    name: "search-results",
    component: SearchResults,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to) => {
  if (to.name === 'AuthUser') {
    return true;
  } else {
    const token = localStorage.getItem('token');
    if (token) {
      return axios.post("http://215c-85-159-24-193.ngrok.io/check", {
        token: token
      }).then((response) => {
        if (response.data.status) {
          return store.dispatch('setUser', response.data.response).then(() => {
            return true;
          });
        } else {
          return "/auth";
        }
      });
    } else {
      return "/auth";
    }
  }
});

export default router;
